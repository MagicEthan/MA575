'''
Created on Feb 5, 2017

@author: ethansong
'''
import numpy as np
import matplotlib.pyplot as plt

fig, ax = plt.subplots()

# Plot the original function
x = np.linspace(-5, 5, 300)
f = lambda x: .5 + np.arctan(x) / np.pi
xinv = np.linspace(0,1,200)
finv = lambda x: np.tan((x - .5)*np.pi)

ax.plot(x, f(x),'r:',linewidth=2.0,label='Cauchy Function')
ax.plot(xinv, finv(xinv),'b-',linewidth=2.0, label ='Cauchy Inv CDF')

# Combine in one and adjust
ax.grid()

ax.axhline(y=0, color='black')
ax.axvline(x=0, color='black')

plt.title('Cauchy VS Inv.Cauchy')
plt.xlabel('x')
plt.ylabel('y')

ax.set_xlim([-3,3])
ax.set_ylim([-3,3])

ax.legend(loc = 'upper left')

plt.show()



