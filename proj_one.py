'''
Created on Feb 15, 2017

@author: ethansong
'''

import numpy as np
# import matplotlib.pyplot as plt
import scipy.stats as spst


# Problem 2(a)
def daily_log_stat(df):
    """Calculate the means and cov matrix of daily log returns for all stocks.
    
    Args:
        df: Dataframe of daily log returns.
        
    Returns:
        np.array(df.mean()): Vector of means.
        np.array(df.cov()): Covariance matrix.
    
    """
    return np.array(df.mean()), np.array(df.cov())

# sdlr_mean, sdlr_cov = daily_log_stat(sdlr) 


# Problem 2(b)
def daily_net_stat(df):
    """Calculate the means and cov matrix of daily net returns for all stocks.
    
    Args:
        df: Dataframe of daily net returns.
        
    Returns:
        np.array(df.mean()): Vector of means. 1*N
        np.array(df.cov()): Covariance matrix. N*N
    
    """
    return np.array(df.mean()), np.array(df.cov())

# sdnr_mean, sdnr_cov = daily_net_stat(sdnr)

def daily_stat(mu, sigma, vt = 1, w = 1/15 * np.ones(15) ):
    """Estimate the mean and variance of daily loss and linearized loss \
    without a presumed distribution.
    
    
    Args:
        mu: Means of daily net/log returns.
        sigma: Covaraince matrix of daily net/log returns.
        vt: Portfolio value at the at the end of yesturday. 
            Default value is 1, which stands for one unit value. 
        w: Weight vector for stocks, by default 1/15 for all.
        
    Returns:
        mean: Mean of daily loss/linearized loss. 1*N
        var: Variance of daily loss/linearizedloss. N*N
    
    """
    mean = -vt * w.dot(mu)
    var = vt ** 2 * w.dot(sigma).dot(w)
    return mean, var

def monthly_stat(mu, sigma, k, vt):
    """Estimate the mean and variance of monthly loss\linearized loss \
    without a presumed distribution.
    
    Args:
        mu: Means of daily net/log returns.
        sigma: Covaraince matrix of daily net/log returns.
        k: Number of trading days in the month.
        vt: Portfolio value at the at the end of last month. 
        
    Returns:
        mmean: Mean of monthly loss/linearized loss.
        mvar: Variance of monthly loss/linearized loss.
    
    """
    dmean, dvar = daily_stat(mu, sigma, vt)
    mmean, mvar = k*dmean, k*dvar
    # You can also calculate monthly aggregated mean and cov first
    # from daily net returns, which will give the same results.
    
    return mmean, mvar


# Problem 2(c)
# Default test data: stocks prices and portfolio value at the end of Dec, 2012
# st0 = np.array([69.56,250.87,354.43,53.36,91.22,38.33,39.17,24.58,31.60,17.536,25.246,131.3275,65.87,38.9,29.78])
# vt = 1271353.46
# stocks shares at the first day of Jan 2013
# shares = np.array([1218.4719250682,337.8518716835,239.1357927769,1588.2809166135,929.1532872809,2211.2748925582,2163.586651396
# ,3447.4241198271,2681.5452960923,4832.8374795117,3357.1359935808,645.3857324508,1286.7710232237,2178.8404370768,2846.1317135612])    


# 1. Normal Distribution Assumption, monthly loss function
# Hybrid methods: Monte Carlo and square root of time scaling (SRTS):
# Use SRTS to scale the stocks prices from 
# daily multivariate normal to monthly multivariate normal
# Then Use Monte Carlo to generate stock price at the end of next month
# And calculate the loss function by definition
# Default stimulation number 100,000

def mc_monthly_loss_normal(mu, sigma, k, st0, vt, shares, n=100000):
    """Estimate monthly loss under nomral distribution assumption.
    
    Args:
        mu: Means of daily log returns.
        sigma: Covaraince matrix of daily log returns.
        k: Number of trading days in the month.
        st0: Stocks prices at the end of last month.
        vt: Portfolio value at the at the end of last month. 
        shares: Stocks shares at the start of the present month.
        n: Number of Monte Carlo simulation.
        
    Returns:
        loss: n simulated loss values of the present month.
    
    """
    st1 = st0 * (np.exp(np.random.multivariate_normal(k*mu, k*sigma, n)))
    loss = -(shares.dot(st1.T) - vt)

    return loss


# 2. Normal Distribution Assumption, monthly linearized loss (linloss) function
# Purely aggregation method, only use MC to generate samples
# Use variance-covariance method to calculate daily loss distribution
# Then apply square-root-of-time scaling

def ag_monthly_linloss_normal(mu, sigma, k, vt, n=100000):
    # mu, sigma = sdlr_mean, sdlr_cov
    dlin_mean, dlin_var = daily_stat(mu, sigma, vt)
    linloss = k*dlin_mean + np.sqrt(k*dlin_var)*np.random.randn(n)

    return linloss


# 3. t-distribution Assumption, monthly loss function
# Hybrid method, VAM and Monte Carlo
# Besides, there is no ready-to-use multivarite t-dist estimator in python
# So we need to write our own's

# First use var-covar method to aggregate log returns of the N stocks
# and get a univariate t-distribution, which has the same degree of freedom 
# as the multivariate t-distribution.

def ag_t_estimator(logreturns, w = 1/15 * np.ones(15)):
    
    data = 1/15 * np.ones(15).dot(logreturns.T)
    dof= spst.t.fit(data[1:]) [0] # The first row is null)
    
    return dof

# The estimated dof is 3.003, here we chose dof=3
# For loss function,
# daily loss mu and sigma can be calculated by the sample data
# dof_t, loc, scale = ag_t_estimator(sdlr)

# Generate multivariate-t distribution r.v.s
def multivariate_t(dof, mu, sigma, n):
    d = len(mu)
    # sigma is cov, not scale
    tao = np.tile(np.random.gamma(dof/2., dof/2., n),(d,1)).T
    Z = np.random.multivariate_normal(np.zeros(d), sigma, n)
    # nXd
    return mu + Z/np.sqrt(tao)

# Monte Carlo Simulation of monthly stocks price movement
def mc_monthly_loss_t(dof, mu, sigma, k, st0, vt, shares, n=100000):
    st1_log = np.zeros([n,15])
    for i in range(n):
        st1_log[i] =  np.sum(multivariate_t(dof, mu, sigma, k), axis = 0)
    st1 = st0 * (np.exp(st1_log))
    loss = -(shares.dot(st1.T) - vt)

    return loss



# 4. t-distribution Assumption, monthly linearized loss function
# Hybrid method, based on previous var-covariance method
# Use Monte Carlo to generate the protoflio value path
def mc_monthly_linloss_t(dof, mu, sigma, k, vt, n=100000):
    # mu, sigma = sdlr_mean, sdlr_cov
    unit_mean, unit_var = daily_stat(mu, sigma)
    unit_mean = -unit_mean # Adjustment for sign
    loc_t = unit_mean
    scale_t = np.sqrt(unit_var/(dof/(dof-2)))
    loss = np.zeros(n)
    for i in range(n):
        vtk = vt * np.prod((1 + loc_t + scale_t * np.random.standard_t(dof, k)))
        loss[i] = -vtk + vt
        
    return loss



# loss_normal = mc_monthly_loss_normal(sdlr_mean, sdlr_cov, 21, st0, vt, shares)
# linloss_normal = ag_monthly_linloss_normal(sdlr_mean, sdlr_cov, 21, vt)
# loss_t = mc_monthly_loss_t(dof_t, sdlr_mean, sdlr_cov, 21, st0, vt, shares)
# linloss_t = mc_monthly_linloss_t(dof_t, sdlr_mean, sdlr_cov, 21, vt)

# data = 1/15 * np.ones(15).dot(sdlr.T)
# a,b,c = spst.t.fit(data[1:],2)
# print(a,b,c,data[1:].mean(),data[1:].std(),-dmean, np.sqrt(dvar))

# fig, ((ax1, ax2),(ax3,ax4)) = plt.subplots(nrows=2, ncols=2)
# print("Mean and Std:")
# print(np.mean(loss_normal), np.std(loss_normal))
# print(np.mean(linloss_normal), np.std(linloss_normal))
# print(np.mean(loss_t), np.std(loss_t))
# print(np.mean(linloss_t), np.std(linloss_t))
# ax1.hist(loss_normal, range = [-300000, 300000],bins=np.linspace(-300000,300000,500))
# ax2.hist(linloss_normal, range = [-300000, 300000],bins=np.linspace(-300000,300000,500))
# ax3.hist(loss_t, range = [-300000, 300000],bins=np.linspace(-300000,300000,500))
# ax4.hist(linloss_t, range = [-300000, 300000],bins=np.linspace(-300000,300000,500))
# ax1.set_title('loss_normal')
# ax2.set_title('linloss_normal')
# ax3.set_title('loss_t')
# ax4.set_title('linloss_t')
# plt.show()
