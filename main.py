'''
Created on Feb 19, 2017

@author: ethansong
'''
import proj_one as pj
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import matplotlib.ticker as ticker

# 2(a)
# Initializing data
# Import training data of 2011-2012
pf_train = pd.read_excel('2011_2012.xlsx')
# Create a dataframe of daily stocks price
stocks = pf_train.iloc[:,1:-3:2]
# Calculate stocks daily net returns
sdnr = stocks.diff() / stocks.shift(1)
# Calculate stocks daily log returns
sdlr = np.log(stocks / stocks.shift(1))

# Calculate the daily mean a covariance matrix of the daily log returns
sdlr_mean, sdlr_cov = pj.daily_log_stat(sdlr) 
# np.savetxt("2a_mean.csv", sdlr_mean, delimiter=' & ', fmt='%4.4e', newline=' \\\\\n')
# np.savetxt("2a_cov.csv", sdlr_cov, delimiter=' & ', fmt='%4.4e', newline=' \\\\\n')


# 2(b)
# Calculate the daily mean a covariance matrix of the daily net returns
sdnr_mean, sdnr_cov = pj.daily_log_stat(sdnr) 
# Use Jan 2013 as an example
vt = 1271353.46 # Close price at 12/13/2012
k = 21 # Number of trading days in Jan 2013
jan2013_loss_mean, jan2013_loss_var  = pj.monthly_stat(sdnr_mean, sdnr_cov, k, vt)
# print(jan2013_loss_mean,np.sqrt(jan2013_loss_var))
jan2013_linloss_mean, jan2013_linloss_var  = pj.monthly_stat(sdlr_mean, sdlr_cov, k, vt)
# print(jan2013_linloss_mean, np.sqrt(jan2013_linloss_var))

# 2(c)(d)
def second_cd():
    # Stocks shares in Jan 2013
    shares = np.array([1218.4719250682,337.8518716835,239.1357927769,1588.2809166135,929.1532872809,2211.2748925582,2163.586651396
    ,3447.4241198271,2681.5452960923,4832.8374795117,3357.1359935808,645.3857324508,1286.7710232237,2178.8404370768,2846.1317135612])    
    # Stocks close price at 12/31/2012
    st0 = np.array([69.56,250.87,354.43,53.36,91.22,38.33,39.17,24.58,31.60,17.536,25.246,131.3275,65.87,38.9,29.78])
    dof_t= pj.ag_t_estimator(sdlr)
    # print("Degrees of freedom is ", dof_t)
    sns.set(color_codes=True)
    
    loss_normal = pj.mc_monthly_loss_normal(sdlr_mean, sdlr_cov, k, st0, vt, shares)
    fig, ax = plt.subplots()
    ax = sns.distplot(loss_normal[abs(loss_normal) <=400000 ], bins = np.linspace(-400000,400000,81))
    ax.set_title('Loss - Normal')
    # Set the gridlines interval
    ax.xaxis.set_major_locator(ticker.MultipleLocator(100000))
    plt.savefig('Loss - Normal')
    plt.close()
    
    linloss_normal = pj.ag_monthly_linloss_normal(sdlr_mean, sdlr_cov, k, vt)
    fig, ax = plt.subplots()
    ax = sns.distplot(linloss_normal[linloss_normal <=400000], bins = np.linspace(-400000,400000,81))
    ax.set_title('Linearized Loss - Normal')
    ax.xaxis.set_major_locator(ticker.MultipleLocator(100000))
    plt.savefig('Linearized Loss - Normal')
    plt.close()
    
    loss_t = pj.mc_monthly_loss_t(dof_t, sdlr_mean, sdlr_cov, k, st0, vt, shares)
    fig, ax = plt.subplots()
    ax = sns.distplot(loss_t[abs(loss_t) <=400000 ], bins = np.linspace(-400000,400000,81))
    ax.set_title('Loss - t')
    ax.xaxis.set_major_locator(ticker.MultipleLocator(100000))
    plt.savefig('Loss - t')
    plt.close()
        
    linloss_t = pj.mc_monthly_linloss_t(dof_t, sdlr_mean, sdlr_cov, k, vt)
    fig, ax = plt.subplots()
    ax = sns.distplot(linloss_t[abs(linloss_t) <=400000 ], bins = np.linspace(-400000,400000,81))
    ax.xaxis.set_major_locator(ticker.MultipleLocator(100000))
    ax.set_title('Linearized Loss - t')
    plt.savefig('Linearized Loss - t')
    plt.close()
     
    fig, ((ax1, ax2),(ax3,ax4)) = plt.subplots(nrows=2, ncols=2)
    print("Mean and Std:")
    print(np.mean(loss_normal), np.std(loss_normal))
    print(np.mean(linloss_normal), np.std(linloss_normal))
    print(np.mean(loss_t), np.std(loss_t))
    print(np.mean(linloss_t), np.std(linloss_t))
    sns.distplot(loss_normal[abs(loss_normal) <=400000 ], bins = np.linspace(-400000,400000,81),ax=ax1)
    sns.distplot(linloss_normal[linloss_normal <=400000], bins = np.linspace(-400000,400000,81),ax=ax2)
    sns.distplot(loss_t[abs(loss_t) <=400000 ], bins = np.linspace(-400000,400000,81),ax=ax3)
    sns.distplot(linloss_t[abs(linloss_t) <=400000 ], bins = np.linspace(-400000,400000,81),ax=ax4)
     
    ax1.set_title('Loss - Normal')
    ax2.set_title('Linloss - Normal')
    ax3.set_title('Loss - t')
    ax4.set_title('Linloss - t')
    ax1.xaxis.set_major_locator(ticker.MultipleLocator(100000))
    ax2.xaxis.set_major_locator(ticker.MultipleLocator(100000))
    ax3.xaxis.set_major_locator(ticker.MultipleLocator(100000))
    ax4.xaxis.set_major_locator(ticker.MultipleLocator(100000))
    
    plt.show()

# second_cd()

# 3(a)
# Import test data
pf_test = pd.read_excel('2013_2016.xlsx')
# Create dataframes for dailiy portoflii ovalue and daily log-returns
pf_t = pf_test['Date']
pf_value = pf_test['Vt']
pf_lr = pf_test['Log_Ret_Daily']
pf_value_plot = pf_value / 1000000
fig, ax = plt.subplots()
ax.plot(pf_t, pf_value_plot, color = 'red', linewidth = 2, label = 'Portfolio Value (Million)')
ax.plot(pf_t, pf_lr, color = 'blue', linewidth = 1, label = 'Log Returns')
ax.set_title('Portfolio Value & Log Returns VS Time')
ax.set_ylabel('Value')
ax.set_xlabel('Date')
# plt.show()
plt.close()


# 3(b)(c)
# used Excel directly to calculate the monthly loss, see the spreadsheets
ml_test = pd.read_excel('2013_2016_Month_Loss.xlsx')
ml_loss = ml_test['Loss']
fig, ax = plt.subplots()
ax.hist(ml_loss, range = [-120000, 120000],bins=np.linspace(-220000,220000,45))
ax.set_ylabel('Number')
ax.set_xlabel('Loss')
ax.set_title('Realized Monthly Loss, 2013-2016')
# plt.show()
plt.close()

# 3(d)
# Calculate 48 months' 4 loss distributions using rolling windows
# def get_loss_functions(data):
def test_loss(sheet,k,st0,vt,shares):
    pf = pd.read_excel('test_windows.xlsx',sheetname=sheet)
    # Create a dataframe of daily stocks price
    stocks = pf.iloc[:,1:-3:2]
    # Calculate stocks daily log returns
    sdlr = np.log(stocks / stocks.shift(1))
    # Calculate the daily mean a covariance matrix of the daily log returns
    sdlr_mean, sdlr_cov = pj.daily_log_stat(sdlr) 
    dof_t= pj.ag_t_estimator(sdlr)
    loss_normal = pj.mc_monthly_loss_normal(sdlr_mean, sdlr_cov, k, st0, vt, shares)
    linloss_normal = pj.ag_monthly_linloss_normal(sdlr_mean, sdlr_cov, k, vt)
    loss_t = pj.mc_monthly_loss_t(dof_t, sdlr_mean, sdlr_cov, k, st0, vt, shares)
    linloss_t = pj.mc_monthly_linloss_t(dof_t, sdlr_mean, sdlr_cov, k, vt)
    print("Degrees of freedom: ", dof_t)
    print("Mean and Std:")
    print(np.mean(loss_normal), np.std(loss_normal))
    print(np.mean(linloss_normal), np.std(linloss_normal))
    print(np.mean(loss_t), np.std(loss_t))
    print(np.mean(linloss_t), np.std(linloss_t))
    np.savetxt('loss_normal_'+sheet +'.txt',  loss_normal, fmt = '%.4f',)
    np.savetxt('linloss_normal_'+sheet +'.txt',  linloss_normal, fmt = '%.4f',)
    np.savetxt('loss_t_'+sheet+'.txt',  loss_t, fmt = '%.4f',)
    np.savetxt('linloss_t_'+sheet+'.txt',  linloss_t, fmt = '%.4f',)

vt = 1352421.91661689 # Close price at last day of last month
k = 20 # Number of trading days in the estimating month
# Stocks shares in the estimating month
shares = np.array([1553.6590897396
,341.1717745513
,237.1733855868
,1545.0528924039
,960.855872418
,2189.0859640514
,2054.5511194796
,3461.2348595135
,2775.8899608929
,4876.1874147509
,3478.6999868892
,644.8785028306
,1212.0591519885
,1919.9630116643
,2737.1863172712
])    
# Stocks close price at the last day of last month
st0 = np.array([58.031689
,264.269989
,380.149994
,58.354935
,93.834532
,41.186807
,43.883776
,26.048929
,32.4802
,18.490155
,25.918148
,139.811547
,74.387014
,46.959999
,32.939468
])  

test_loss('Mar2013',k,st0,vt,shares)
# np.set_printoptions(suppress = True)

def var_cvar(dist, months, u):
    var = np.array([])
    cvar = np.array([])
    for m in months:
        loss = pd.read_csv(dist + m +'.txt', header = None)
        var_m = np.percentile(loss,u)
        var = np.append(var, var_m)
        cvar_m = loss[loss>=var_m].mean()
        cvar = np.append(cvar, cvar_m)
    
    np.savetxt(dist + 'Var.txt', var, fmt = '%.4f',)
    np.savetxt(dist + 'CVar.txt', var, fmt = '%.4f',)
    return var , cvar

# var, cvar = var_cvar('linloss_t_',['Feb2013','Jan2013','Mar2013'],95)
# print(var)
# print(cvar)
