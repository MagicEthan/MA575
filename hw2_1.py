'''
Created on Feb 5, 2017

@author: ethansong
'''
import numpy as np
import matplotlib.pyplot as plt

fig, ax = plt.subplots()

# Plot the original function
x1 = np.linspace(-4, -1, 200)
x2 = np.linspace(-1, 1, 200)
x3 = np.linspace(1, 3, 200)

f1 = lambda x: -1 / x
f2 = np.ones(200)
f3 = lambda x: x ** 2

ax.plot(x1, f1(x1),'r:',linewidth=2.0)
ax.plot(x2, f2,'r:',linewidth=2.0)
ax.plot(x3, f3(x3),'r:', linewidth=2.0, label = 'Original')

# Plot the generalized inverse function
x1 = np.linspace(0.1, 1, 100)
x3 = np.linspace(1, 6, 400)
finv1 = lambda x: -1 / x
finv3 = lambda x: x ** 0.5

ax.plot(x1, finv1(x1),'b-')
ax.plot(1,1,'o',mfc='none') # Point(1,1) not on the graph
ax.plot(x3, finv3(x3),'b-', label = 'General.Inv')

# Combine in one and adjust
ax.grid()

ax.axhline(y=0, color='black')
ax.axvline(x=0, color='black')

plt.title('Generalized Inverse VS Original')
plt.xlabel('x')
plt.ylabel('y')

ax.set_xlim([-4,4])
ax.set_ylim([-4,4])

ax.legend(loc = 'upper left')

plt.show()



