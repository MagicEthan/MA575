function[rvs] = gaussianCopulaRVs(lbd1, lbd2, nu1, sigma, num)
% Author: Yupu Song, Khasan Dymov
% Date: Apr 2, 2017
% Generate dependet random variables using gassian copula
% Input: 
% lbd1, lbd2: lambda parameters of the two exponential distributions, 
% equal to the mean values of the distributions
% nu1: degrees of freedom of the two chi-squared distributions
% sigma: the covariance matrix for the rvs
% num: number of rvs to be generated

% Convert the covariance matrix to correlation matrix
corr = corrcov(sigma);

% Draw samples from continuous multivariate uniform distributions
% with the copula whose dependence structure is determined by the
% correlation matrix
u = copularnd('gaussian', corr, num);

% Use inverse transform method to transform the uniform marginal
% distributions in each column into specific distributions
rvs = [expinv(u(:,1), 1/lbd1),expinv(u(:,2),1/lbd2),...
    chi2inv(u(:,3),nu1), chi2inv(u(:,4),nu1), norminv(u(:,5),0,1)];
 
